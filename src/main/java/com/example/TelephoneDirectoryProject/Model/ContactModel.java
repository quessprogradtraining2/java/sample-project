package com.example.TelephoneDirectoryProject.Model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Contacts")
public class ContactModel {
    @Id
    private long phoneNumber;
    private String contactName;
    public ContactModel() {
    }

    public ContactModel(String contactName, long phoneNumber) {
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
    }

    public String getContactName() {

        return contactName;
    }

    public void setContactName(String contactName) {

        this.contactName = contactName;
    }

    public long getPhoneNumber() {

        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

}

package com.example.TelephoneDirectoryProject.Controller;

import com.example.TelephoneDirectoryProject.Model.ContactModel;
import com.example.TelephoneDirectoryProject.Service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;

@RestController
public class ContactController {
    @Autowired
    ContactService contactServiceObj;

    @PostMapping("/insertContactDetails")
    public String addContact(@RequestBody ContactModel contactModelObj){
        String str=contactServiceObj.addContacts(contactModelObj);
        return str;
    }

    @GetMapping("/viewcontacts")
    public List<ContactModel> displayAllContacts(){
        return contactServiceObj.getContacts();
    }


    @DeleteMapping("/deletecontactbyid/{phoneNumber}")
    public String deleteContact(@PathVariable long phoneNumber){
        contactServiceObj.deleteContactByphoneNumber(phoneNumber);
        return " Contact Deleted";
    }

    @PutMapping("/updatecontactphonenumber/{phoneNumber}")
    public String updateContactPhoneNumber(@RequestBody ContactModel contactModelObj, @PathVariable long phoneNumber){
        contactServiceObj.updatePhoneNumber(contactModelObj, phoneNumber);
        return "Updated";
    }
}

package com.example.TelephoneDirectoryProject.Service;

import com.example.TelephoneDirectoryProject.Model.ContactModel;
import com.example.TelephoneDirectoryProject.Model.UserModel;
import com.example.TelephoneDirectoryProject.Repository.UserRepository;
import com.example.TelephoneDirectoryProject.Security.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepositoryObj;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserModel userModel=userRepositoryObj.findByUserName(userName);
        if( userModel == null){
            throw new UsernameNotFoundException("Username not exsists");
        }
        return new MyUserDetails(userModel);
    }
}

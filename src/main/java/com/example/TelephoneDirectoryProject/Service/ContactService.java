package com.example.TelephoneDirectoryProject.Service;

import com.example.TelephoneDirectoryProject.Model.ContactModel;
import com.example.TelephoneDirectoryProject.Repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Transactional
@Service
public class ContactService {
    @Autowired
    ContactRepository contactRepositoryObj;
    public String addContacts(ContactModel contactModelObj) {
        contactRepositoryObj.save(contactModelObj);
        return "Contact is Added";
    }

    public List<ContactModel> getContacts() {
        return contactRepositoryObj.findAll();
    }


    public void updatePhoneNumber(ContactModel contactModelObj, long phoneNumber) {
        ContactModel fetchContactModelObj=contactRepositoryObj.findByPhoneNumber(phoneNumber).get();
        if(fetchContactModelObj!=null){
            contactRepositoryObj.delete(fetchContactModelObj);
            contactRepositoryObj.save(contactModelObj);
        }
    }


    public void deleteContactByphoneNumber(long phoneNumber) {
        contactRepositoryObj.deleteByPhoneNumber(phoneNumber);
    }
}

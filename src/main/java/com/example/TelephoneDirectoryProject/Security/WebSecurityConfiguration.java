package com.example.TelephoneDirectoryProject.Security;

import com.example.TelephoneDirectoryProject.Service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.net.Authenticator;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Autowired
    public UserDetailsService userDetailsService;
        AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable();
            http.cors().disable();
            http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/viewcontacts").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/insertContactDetails").hasAuthority("ADMIN")
                    .antMatchers("/deletecontactbyid/{phoneNumber}").hasAuthority("ADMIN")
                    .antMatchers("/updatecontactphonenumber/{phoneNumber}").hasAuthority("ADMIN")
                .anyRequest().authenticated().and().httpBasic();
    }
}

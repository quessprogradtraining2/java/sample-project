package com.example.TelephoneDirectoryProject.Repository;

import com.example.TelephoneDirectoryProject.Model.ContactModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface ContactRepository extends JpaRepository<ContactModel, Integer> {
    Optional<ContactModel> findByPhoneNumber(Long phoneNumber);


    void deleteByPhoneNumber(long phoneNumber);
}

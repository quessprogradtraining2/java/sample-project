package com.example.TelephoneDirectoryProject.Repository;

import com.example.TelephoneDirectoryProject.Model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserModel,Integer> {
    UserModel findByUserName(String userName);
}

package com.example.TelephoneDirectoryProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelephoneDirectoryProjectApplication {

	public static void main(String[] args) {

		SpringApplication.run(TelephoneDirectoryProjectApplication.class, args);
	}

}
